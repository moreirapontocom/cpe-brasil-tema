<?php
function wef_scripts() {
	wp_enqueue_style('cpeboostrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css','','all');
	wp_enqueue_style('cpefontawesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css','','all');
	wp_enqueue_style('cpestyle',get_template_directory_uri().'/style.css','cpeboostrap','','all');
	wp_enqueue_style('cpefancybox',get_template_directory_uri().'/plugins/fancyBox/source/jquery.fancybox.css','cpestyle','','all');

	wp_enqueue_script('cpejquery','https://code.jquery.com/jquery-1.12.3.min.js','',true);
	wp_enqueue_script('cpebootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',array('cpejquery'),'',true);
	wp_enqueue_script('cpefancybox',get_template_directory_uri().'/plugins/fancyBox/source/jquery.fancybox.pack.js',array('cpejquery'),'',true);
	wp_enqueue_script('cpescripts',get_template_directory_uri().'/scripts.js',array('cpejquery'),'',true);
}
add_action( 'wp_enqueue_scripts', 'wef_scripts' );

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Menu principal' ));
}
add_action( 'init', 'register_my_menu' );

function custom_theme_setup() {
	// add_theme_support('post-thumbnails');
	add_theme_support('post-formats');
}
add_action( 'after_setup_theme', 'custom_theme_setup' );

add_action( 'widgets_init', 'widgets_sidebar' );
function widgets_sidebar() {
	register_sidebar( array(
		'name'			=> __( 'Rodapé 1', 'cpe-brasil' ),
		'id'			=> 'footer-widget-1',
		'description'	=> __( 'Sobre a empresa', 'cpe-brasil' ),
		'before_widget' => '<div class="col col-xs-12 col-sm-4 col-lg-5 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Rodapé 2', 'cpe-brasil' ),
		'id'			=> 'footer-widget-2',
		'description'	=> __( 'Newsletter', 'cpe-brasil' ),
		'before_widget' => '<div class="col col-xs-12 col-sm-4 col-md-4 col-lg-4 newsletter %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Rodapé 3', 'cpe-brasil' ),
		'id'			=> 'footer-widget-3',
		'description'	=> __( 'Menu', 'cpe-brasil' ),
		'before_widget' => '<div class="col col-xs-12 col-sm-4 col-lg-3 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	) );
}

add_action('init', 'register_mypost_type');
function register_mypost_type() {

	register_post_type('produtos', array(
		'hierarchical' => true,
		'rewrite' => array("slug" => "produto"),
		'label' => __('Produtos'),
		'singular_label' => __('Produto'),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'query_var' => "event",
		'supports' => array('title','editor','slug','category'),
		'taxonomies' => array('category'),
		'has_archive' => false
	));

}

// highlight active custom post page in nav
add_filter( 'nav_menu_css_class', 'highlight_current_menu_custom_post_type', 10, 2 );
function highlight_current_menu_custom_post_type( $classes , $item ){
	if ( get_post_type() == 'produtos' ) {
		if ( strpos($item->url, 'produto') )
			$classes = str_replace( 'menu-item', 'menu-item current_page_parent current-menu-item', $classes );
	}
	return $classes;
}

/**
 * O plugin ACF - Advanced Custom Fields - faz parte deste tema premium
 * Voce NÃO pode redistribuir o plugin ACF separadamente deste tema
 */
// 1. customize ACF path

add_filter('acf/settings/path', 'my_acf_settings_path');
function my_acf_settings_path( $path ) {
    $path = get_stylesheet_directory() . '/plugins/acf/';
    return $path;
}

// 2. customize ACF dir
add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir( $dir ) {
    $dir = get_stylesheet_directory_uri() . '/plugins/acf/';
    return $dir;

}

// 3. Hide ACF field group menu item
add_filter('acf/settings/show_admin', '__return_false');

// 4. Include ACF
include_once( get_stylesheet_directory() . '/plugins/acf/acf.php' );

if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Opções do Tema',
		'menu_title'	=> 'Opções do Tema',
		'menu_slug' 	=> 'setup-theme',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position'		=> 58
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Banner',
		'menu_title' 	=> 'Banner',
		'parent_slug' 	=> 'setup-theme',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Categorias dos produtos',
		'menu_title' 	=> 'Categorias',
		'parent_slug' 	=> 'setup-theme',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Parceiros',
		'menu_title' 	=> 'Parceiros',
		'parent_slug' 	=> 'setup-theme',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Unidades',
		'menu_title' 	=> 'Unidades',
		'parent_slug' 	=> 'setup-theme',
	));



if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_579034da518dc',
		'title' => 'Banner',
		'fields' => array (
			array (
				'key' => 'field_5791a78dfa27c',
				'label' => 'Banner das páginas',
				'name' => 'banner_das_paginas',
				'type' => 'image',
				'instructions' => 'Estes banners serão exibidos no topo de cada página interna do site, atrás do título da página. (Tamanho da imagem: 1420px largura x 193px altura)',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
				'min_width' => 1420,
				'min_height' => 193,
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array (
				'key' => 'field_579034da5ac86',
				'label' => 'Imagens',
				'name' => 'banner_repeater',
				'type' => 'repeater',
				'instructions' => 'Estes banners serão exibidos na página principal, em formato de slider. As imagens se alternam automaticamente entre si.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'row',
				'button_label' => 'Adicionar Linha',
				'sub_fields' => array (
					array (
						'key' => 'field_579034da665ae',
						'label' => 'Imagem do banner',
						'name' => 'imagem_do_banner',
						'type' => 'image',
						'instructions' => 'Escolha uma imagem para o banner. (Tamanho da imagem: 1420px largura x 400px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => 1420,
						'min_height' => 400,
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array (
						'key' => 'field_579034da66677',
						'label' => 'Texto do banner',
						'name' => 'texto_do_banner',
						'type' => 'textarea',
						'instructions' => 'Escreva um texto que será exibido no banner.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => 3,
						'new_lines' => 'wpautop',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_579034da66737',
						'label' => 'Texto do botão do banner',
						'name' => 'texto_do_botao_do_banner',
						'type' => 'text',
						'instructions' => 'Texto que será exibido no botão do banner. Exemplos: "Leia mais", "Saiba como" ou "Clique aqui".',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => 'Leia mais',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_579034da667f6',
						'label' => 'Link do botão do banner',
						'name' => 'link_do_botao_do_banner',
						'type' => 'page_link',
						'instructions' => 'Escolha a página que será exibida ao clicar no botão do banner.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'post_type' => array (
						),
						'taxonomy' => array (
						),
						'allow_null' => 0,
						'multiple' => 0,
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-banner',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_5790d26870804',
		'title' => 'Categorias',
		'fields' => array (
			array (
				'key' => 'field_5790d2fd00545',
				'label' => 'Categorias',
				'name' => 'categorias_dos_produtos',
				'type' => 'repeater',
				'instructions' => 'Esta é a configuração avançada das categorias que são exibidas na página inicial do site.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => 'field_5790d31200546',
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Adicionar Categoria',
				'sub_fields' => array (
					array (
						'key' => 'field_5790d31200546',
						'label' => 'Nome da categoria',
						'name' => 'nome_da_categoria',
						'type' => 'text',
						'instructions' => 'Escreva um nome para esta categoria.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5790d33600548',
						'label' => 'Foto de capa',
						'name' => 'foto_de_capa_da_categoria',
						'type' => 'image',
						'instructions' => 'Selecione uma imagem para servir como capa desta categoria.
	(Tamanho da imagem: 263px largura x 240px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => 263,
						'min_height' => 240,
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-categorias',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_5790302c6b5fe',
		'title' => 'Geral',
		'fields' => array (
			array (
				'key' => 'field_57903078c2163',
				'label' => 'Contatos',
				'name' => 'contatos',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'left',
				'endpoint' => 0,
			),
			array (
				'key' => 'field_579035a386534',
				'label' => 'Telefone',
				'name' => 'telefone',
				'type' => 'text',
				'instructions' => 'Este campo aparecerá no topo do site. O melhor formato é (ddd) 0000-0000.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_579035c686535',
				'label' => 'E-mail',
				'name' => 'email',
				'type' => 'email',
				'instructions' => 'Este campo aparecerá no topo do site.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_57903338781ed',
				'label' => 'Institucional',
				'name' => '',
				'type' => 'tab',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'placement' => 'left',
				'endpoint' => 0,
			),
			array (
				'key' => 'field_5790337e781ee',
				'label' => 'Nossa visão',
				'name' => 'visao_content',
				'type' => 'textarea',
				'instructions' => 'Este campo aparecerá na página inicial do site, na área com um fundo azul semi-transparente. Escreva sobre a visão da empresa.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 3,
				'new_lines' => 'wpautop',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_579033c6781ef',
				'label' => 'Nossa missão',
				'name' => 'missao_content',
				'type' => 'textarea',
				'instructions' => 'Este campo aparecerá na página inicial do site, na área com um fundo azul semi-transparente. Escreva sobre a missão da empresa.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 3,
				'new_lines' => 'wpautop',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'setup-theme',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_57904ab14e6aa',
		'title' => 'Onde estamos',
		'fields' => array (
			array (
				'key' => 'field_57904abd9a1e7',
				'label' => 'Endereço completo',
				'name' => 'endereco_completo',
				'type' => 'textarea',
				'instructions' => 'Escreva o endereço completo desta unidade CPE.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 3,
				'new_lines' => 'wpautop',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_57904ad99a1e8',
				'label' => 'Imagem Google Maps',
				'name' => 'imagem_google_maps',
				'type' => 'image',
				'instructions' => 'Selecione a imagem do mapa de localização desta unidade CPE.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'preview_size' => 'full',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-contato.php',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_5790370c03675',
		'title' => 'Parceiros',
		'fields' => array (
			array (
				'key' => 'field_579037102670b',
				'label' => 'Nossos parceiros',
				'name' => 'parceiros_repeater',
				'type' => 'repeater',
				'instructions' => 'Área com a lista de parceiros da CPE.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Adicionar Linha',
				'sub_fields' => array (
					array (
						'key' => 'field_579037262670c',
						'label' => 'Logo do parceiro',
						'name' => 'logo_do_parceiro',
						'type' => 'image',
						'instructions' => 'Selecione uma imagem para este parceiro (geralmente é a logo).
	(Tamanho da imagem: 263px largura x 100px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => 263,
						'min_height' => 100,
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array (
						'key' => 'field_579037522670d',
						'label' => 'URL do site',
						'name' => 'url_do_site_parceiro',
						'type' => 'url',
						'instructions' => 'Insira o link para o site do parceiro no formato: http://link-do-site.com.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-parceiros',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_579030fc43833',
		'title' => 'Produtos',
		'fields' => array (
			array (
				'key' => 'field_5790cd8b615df',
				'label' => 'Mais informações',
				'name' => 'produtos_repeater',
				'type' => 'repeater',
				'instructions' => 'Informações detalhadas sobre este produto.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'row',
				'button_label' => 'Adicionar Produto',
				'sub_fields' => array (
					array (
						'key' => 'field_5790d14717bab',
						'label' => 'Material',
						'name' => 'material_produto',
						'type' => 'text',
						'instructions' => 'Em qual material este produto é feito.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => 50,
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5790d15d17bac',
						'label' => 'Aplicação',
						'name' => 'aplicacao',
						'type' => 'text',
						'instructions' => 'Este produto foi desenvolvido para ser aplicado em...',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => 400,
						'readonly' => 0,
						'disabled' => 0,
					),
					array (
						'key' => 'field_5790cdad615e1',
						'label' => 'Foto de capa',
						'name' => 'foto_de_capa',
						'type' => 'image',
						'instructions' => 'Escolha a foto que será exibida na capa do produto.
	(Tamanho da imagem: 263px largura x 240px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => 263,
						'min_height' => 240,
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array (
						'key' => 'field_5790cdd9615e2',
						'label' => 'Outras Fotos',
						'name' => 'outras_fotos_repeater',
						'type' => 'repeater',
						'instructions' => 'Selecione uma ou mais fotos para complementar a demonstração do produto.
	(Tamanho da imagem: 263px largura x 240px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'collapsed' => '',
						'min' => '',
						'max' => '',
						'layout' => 'table',
						'button_label' => 'Adicionar Linha',
						'sub_fields' => array (
							array (
								'key' => 'field_5790ce18615e3',
								'label' => 'Foto',
								'name' => 'outra_foto',
								'type' => 'image',
								'instructions' => '',
								'required' => 0,
								'conditional_logic' => 0,
								'wrapper' => array (
									'width' => '',
									'class' => '',
									'id' => '',
								),
								'return_format' => 'url',
								'preview_size' => 'full',
								'library' => 'all',
								'min_width' => 263,
								'min_height' => 240,
								'min_size' => '',
								'max_width' => '',
								'max_height' => '',
								'max_size' => '',
								'mime_types' => '',
							),
						),
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'produtos',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	acf_add_local_field_group(array (
		'key' => 'group_579037b28e1bc',
		'title' => 'Unidades',
		'fields' => array (
			array (
				'key' => 'field_579037b294e66',
				'label' => 'Unidades CPE',
				'name' => 'unidades_repeater',
				'type' => 'repeater',
				'instructions' => 'Área com a lista de unidades da CPE.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => '',
				'max' => '',
				'layout' => 'table',
				'button_label' => 'Adicionar Linha',
				'sub_fields' => array (
					array (
						'key' => 'field_579037b297e2e',
						'label' => 'Logo da unidade',
						'name' => 'logo_da_unidade',
						'type' => 'image',
						'instructions' => 'Logo da unidade.
	(Tamanho da imagem: 263px largura x 100px altura)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'url',
						'preview_size' => 'full',
						'library' => 'all',
						'min_width' => 263,
						'min_height' => 100,
						'min_size' => '',
						'max_width' => '',
						'max_height' => '',
						'max_size' => '',
						'mime_types' => '',
					),
					array (
						'key' => 'field_579037b297f0b',
						'label' => 'URL da unidade',
						'name' => 'url_da_unidade',
						'type' => 'url',
						'instructions' => 'Link para o site desta unidade CPE.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
					),
					array (
						'key' => 'field_57903d78fec0b',
						'label' => 'Região da unidade',
						'name' => 'regiao_da_unidade',
						'type' => 'text',
						'instructions' => 'Região da unidade. Exemplo: "Brasil", "França", etc.',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
						'readonly' => 0,
						'disabled' => 0,
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-unidades',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;


}