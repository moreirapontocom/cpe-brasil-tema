<div class="post-item">

	<style>
		@media all and ( min-width: 992px ) {
			#post-<?php the_ID(); ?> {
				/*background-image: url(<?php the_post_thumbnail_url('full'); ?>);*/
			}
		}
	</style>

	<div class="row" class="post-featured-image-home" id="post-<?php the_ID(); ?>">

		<div class="col-lg-6" style="padding: 0 !important;">
			<img src="<?php the_post_thumbnail_url('full'); ?>" style="margin-top: 0px;">
		</div><!-- /.col -->

		<div class="col-xs-12 col-lg-6 bg-white">

			<div class="post-categories">
		    	<?php echo get_the_category_list(); ?>
		    </div>

			<div class="separator"></div>

		    <h2 class="post-title">
		        <a href="<?php the_permalink(); ?>">
		            <?php the_title(); ?>
		        </a>
		    </h2>

		    <?php get_template_part('post-meta'); ?>

			<?php if ( has_excerpt() ) { ?>

				<div class="separator separator-30"></div>

				<?php the_excerpt(); ?>

			<?php } ?>

			<div class="separator"></div>

		    <a href="<?php the_permalink(); ?>" class="btn btn-default btn-lg btn-read-more">Continue a leitura &raquo;</a>

		</div><!-- /.col -->
	</div><!-- /.row -->

</div><!-- /.post-item --> 