<?php get_header(); ?>

    <section>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8 main-content padding-20 padding-vertical">

					<h1>Você pesquisou por: <b><?php echo $_GET['s']; ?></b></h1>

					<div class="separator separator-40"></div>

					<?php while ( have_posts() ) : the_post(); ?>

						<div class="post-categories">
					    	<?php echo get_the_category_list(); ?>
					    </div>

						<div class="separator"></div>

					    <h2 class="post-title">
					        <a href="<?php the_permalink(); ?>">
					            <?php the_title(); ?>
					        </a>
					    </h2>

					    <?php get_template_part('post-meta'); ?>

						<?php if ( has_post_thumbnail() ) : ?>
							<div class="separator"></div>

							<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php the_title(); ?>">
						<?php endif; ?>

						<div class="separator separator-20"></div>

						<?php echo the_content(); ?>

					<?php endwhile; ?>

                </div><!-- /.col -->
                <div class="col-lg-4 padding-20 padding-vertical">

                    <?php get_sidebar(); ?>

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container -->

    </section>

<?php get_footer(); ?>