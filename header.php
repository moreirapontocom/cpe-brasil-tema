<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

	<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Lucas Moreira - lucasmoreira.com.br">

	<link rel="shortcut icon" href="">

	<style>
		@font-face {
		    font-family: "Myriad";
		    src: url('<?php echo get_template_directory_uri(); ?>/fonts/Myriad/MyriadPro-Regular.otf');
		    font-weight: normal;
		}
		@font-face {
		    font-family: "MyriadBold";
		    src: url('<?php echo get_template_directory_uri(); ?>/fonts/Myriad/MyriadPro-Bold.otf');
		    font-weight: normal;
		}

		.main-banner-page-bg,
		.company-values {
			background-image: url(<?php the_field('banner_das_paginas','option'); ?>) !important;
		}
	</style>

	<?php wp_head(); ?>
</head>
<body>

	<header>
        <div class="container">
            <div class="main-header row">
                <div class="col col-xs-12 col-sm-7 col-lg-7 logo">

                    <a href="<?php bloginfo('home') ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cpe.png" alt="CPE Brasil">
                    </a>

                </div>
                <div class="col col-xs-7 col-sm-5 col-lg-5 hidden-xs">

                    <ul class="list-unstyled">
                        <li>
                            <span class="fa fa-phone"></span>
                        </li>
                        <li class="text-left">
                            <div class="info">
                                <h6>Telefone</h6>
                                <?php the_field('telefone','option'); ?>
                            </div>
                        </li>
                        <li>
                            <span class="fa fa-envelope"></span>
                        </li>
                        <li class="text-left">
                            <div class="info">                                
                                <h6>E-mail</h6>
                                <?php the_field('email','option'); ?>
                            </div>
                        </li>
                    </ul>

                </div>
            </div>
        </div><!-- /.container -->

        <div class="main-menu">
            <div class="container">

                <nav class="navbar navbar-default">
                    <div class="container">

                        <div class="navbar-header">

                            <div class="row">
                                <div class="col col-xs-10 hidden-md hidden-lg">
                                    <ul class="list-unstyled list-inline">
                                        <li class="hidden-xs">
                                            <span class="fa fa-phone"></span>
                                        </li>
                                        <li class="text-left">
                                            <div class="info">
                                                <h6>Telefone</h6>
                                                <?php the_field('telefone','option'); ?>
                                            </div>
                                        </li>
                                        <li class="hidden-xs">
                                            <span class="fa fa-envelope"></span>
                                        </li>
                                        <li class="text-left">
                                            <div class="info">                                
                                                <h6>E-mail</h6>
                                                <?php the_field('email','option'); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- /.col -->
                                <div class="col col-xs-2">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div><!-- /.col -->
                            </div>

                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php
				            	wp_nav_menu( array( 
				            		'theme_location' => 'header-menu',
				            		'container' => false,
				            		'menu_class' => 'nav navbar-nav'
								));
							?>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>

            </div><!-- /.container -->
        </div>
    </header>