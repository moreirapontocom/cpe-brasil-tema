<div class="sidebar-widget widget_search">
    <h2 class="widget-title">Pesquisar</h2>
    
    <form method="get" class="search-form" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">

	    <div class="input-group">
	        <input type="text" class="form-control" placeholder="Pesquisar" name="s" id="s">
	        <span class="input-group-btn">
	            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
	        </span>
	    </div>

	</form>

</div>