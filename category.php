<?php get_header(); ?>

	<div class="main-banner-page">

        <div class="main-banner main-banner-page-bg"></div><!-- /.main-banner -->

        <div class="text-center main-banner-page-content">

				<?php 
				$cat = get_query_var('cat');
				$yourcat = get_category( $cat );
				// echo $yourcat->slug;

				// $category_id = get_cat_ID( get_sub_field('nome_da_categoria','option') );
				// $category_link = get_category_link( get_sub_field('categoria_do_produto','option') );
				// echo $yourcat->cat_ID;
				?>

            <h3><?php echo $yourcat->cat_name; ?></h3>
            <span class="h-separator"></span>
        </div>

    </div>

    <div class="separator separator-50"></div>

    <div class="areas padding-vertical-40">
        <div class="container">

			<div class="row">

				<?php
				$args = array( 'post_type' => 'produtos', 'category_name' => $yourcat->slug, 'orderby'  => array( 'tilte' => 'ASC' ) );
				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) {
					while ( $loop->have_posts() ) {
						$loop->the_post();

						if ( have_rows('produtos_repeater') ) {
						    while ( have_rows('produtos_repeater') ) : the_row(); ?>
								<div class="col col-xs-12 col-sm-6 col-lg-3 the-category">
		
				                    <a href="<?php the_permalink(); ?>">
					                    <div class="the-category-cover" style="background-image: url(<?php the_sub_field('foto_de_capa'); ?>);">
					                        <div class="the-category-name"><?php the_title(); ?></div>
					                    </div>
				                    </a>
		
				                </div><!-- /.col -->
							<?php
							endwhile;
						}
					}
				}

				wp_reset_postdata();
				?>

            </div><!-- /.row -->

            <div class="separator separator-40"></div>

        </div><!-- /.container -->
    </div><!-- /.areas -->

<?php get_footer(); ?>