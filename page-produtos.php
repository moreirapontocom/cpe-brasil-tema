<?php /* Template Name: Produtos */ ?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="main-banner-page">

	        <div class="main-banner main-banner-page-bg"></div><!-- /.main-banner -->

	        <div class="text-center main-banner-page-content">
	            <h3><?php the_title(); ?></h3>
	            <span class="h-separator"></span>
	        </div>

	    </div>

	    <div class="separator separator-50"></div>

	    <div class="areas padding-vertical-40">
	        <div class="container">

                <?php the_content(); ?>

				<?php if ( get_the_content() ) { ?>
					<div class="separator separator-40"></div>
				<?php } ?>

				<?php
				$categories = get_categories('post_type=produtos');
				$total_categories = count($categories);
				?>

	            <h3 class="categories-list">
	            	<!-- page-produtos.php -->
	                <ul class="list-unstyled list-inline">
	                    <li>Categorias: </li>
	                    <li>
	                    	<?php for ( $c=0;$c<$total_categories;++$c ) { ?>
	                    		<?php $category_link = get_category_link( $categories[$c]->cat_ID ); ?>
	                        	<a href="<?php echo esc_url( $category_link ); ?>"><?php echo $categories[$c]->name; ?></a> 
	                        <?php } ?>
	                    </li>
	                </ul>
	            </h3>

            	<div class="separator separator-40"></div>

	            <div class="row">

					<?php
					$args = array( 'post_type' => 'produtos', 'orderby'  => array( 'tilte' => 'ASC' ) );
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) {
							$loop->the_post(); ?>

							<div class="col col-xs-12 col-sm-6 col-lg-3 the-category">

									<?php
									if ( have_rows('produtos_repeater') ) {
									    while ( have_rows('produtos_repeater') ) : the_row();
									        ?>
									        <a href="<?php echo get_the_permalink(); ?>">
							                    <div class="the-category-cover" style="background-image: url(<?php the_sub_field('foto_de_capa'); ?>);">
							                        <div class="the-category-name"><?php echo the_title(); ?></div>
							                    </div>
						                    </a>
									        <?php

									    endwhile;
									}
									?>

			                	</div><!-- /.col -->

							<?php
							}
						}

					wp_reset_postdata(); ?>

				</div><!-- /.row -->

	            <div class="separator separator-40"></div>

	        </div><!-- /.container -->
	    </div><!-- /.areas -->

	<?php endwhile; ?>

<?php get_footer(); ?>