<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="main-banner-page">

	        <div class="main-banner main-banner-page-bg"></div><!-- /.main-banner -->

	        <div class="text-center main-banner-page-content">
	            <h3><?php the_title(); ?></h3>
	            <span class="h-separator"></span>
	        </div>

	    </div>

	    <div class="separator separator-50"></div>

	    <div class="areas padding-vertical-40">
	        <div class="container">


				<?php
				if ( have_rows('produtos_repeater') ) {
				    while ( have_rows('produtos_repeater') ) : the_row(); ?>

						<div class="col col-xs-6 col-sm-6 col-md-4 col-lg-2">
		                    <a class="fancybox" rel="group" href="<?php echo get_sub_field('foto_de_capa'); ?>">
		                    	<img src="<?php echo get_sub_field('foto_de_capa'); ?>" alt="<?php the_title(); ?>">
	                    	</a>
		                </div><!-- /.col -->

			    		<?php if ( have_rows('outras_fotos_repeater') ) : ?>

				    		<div class="row product-thumbnails">

				    			<?php while ( have_rows('outras_fotos_repeater') ) : the_row(); ?>

					                <div class="col col-xs-6 col-sm-6 col-md-4 col-lg-2">
					                    <a class="fancybox" rel="group" href="<?php echo get_sub_field('outra_foto'); ?>">
					                    	<img src="<?php echo get_sub_field('outra_foto'); ?>" alt="<?php the_title(); ?>">
				                    	</a>
					                </div><!-- /.col -->

								<?php endwhile; ?>

				            </div><!-- /.row -->

			            <?php endif; ?>


			            <div class="separator separator-40"></div>

			            <div class="row">

			                <div class="col col-lg-4">

			                    <h3>Características</h3>

			                    <div class="separator separator-40"></div>

			                    <table class="table table-bordered">
			                        <tr>
			                            <th>Aplicação:</th>
			                            <td><?php echo get_sub_field('aplicacao'); ?></td>
			                        </tr>
			                        <tr>
			                            <th>Material:</th>
			                            <td><?php echo get_sub_field('material_produto'); ?></td>
			                        </tr>
			                    </table>

			                </div><!-- /.col -->
			                <div class="col col-lg-8">

			                    <h3>Detalhes</h3>

			                    <div class="separator separator-40"></div>

			                    <?php the_content(); ?> 

			                </div><!-- /.col -->

			            </div><!-- /.row -->

		            <?php endwhile;
				}
				?>


				<div class="separator separator-80"></div>

				<h3>Produtos Relacionados</h3>

				<div class="separator separator-40"></div>

				<?php
				$category = get_the_category(); 
				$the_category = $category[0]->cat_ID;

				$related = array(
					'post_type' => 'produtos',
					'cat' => $the_category,
					'post_per_page' => 4,
					'post__not_in' => array(get_the_ID())
				);

				$query = new WP_Query( $related );
				if ( $query->have_posts() ) {
					?>
					<div class="row">
						<?php
						while ( $query->have_posts() ) : $query->the_post(); ?>

							<div class="col col-xs-12 col-sm-6 col-lg-3 the-category">

								<?php if ( have_rows('produtos_repeater') ) : ?>

									<?php while ( have_rows('produtos_repeater') ) :
										the_row(); ?>

								        <a href="<?php echo get_the_permalink(); ?>">
								            <div class="the-category-cover" style="background-image: url(<?php echo get_sub_field('foto_de_capa'); ?>);">
								                <div class="the-category-name"><?php echo get_the_title(); ?></div>
								            </div>
								        </a>

							        <?php endwhile; ?>

						        <?php endif; ?>
						    </div><!-- /.col -->

						<?php
						endwhile;

						wp_reset_postdata();
						?>
					</div><!-- /.row -->
					<?php
				}
				?>

	            <div class="separator separator-40"></div>

	        </div><!-- /.container -->
	    </div><!-- /.areas -->

	<?php endwhile; ?>

<?php get_footer(); ?>