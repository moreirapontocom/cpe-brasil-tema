<?php /* Template Name: Contato */ ?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="main-banner-page">

	        <div class="main-banner main-banner-page-bg"></div><!-- /.main-banner -->

	        <div class="text-center main-banner-page-content">
	            <h3><?php the_title(); ?></h3>
	            <span class="h-separator"></span>
	        </div>

	    </div>

	    <div class="separator separator-50"></div>

	    <div class="areas padding-vertical-40">
	        <div class="container">
	
	            <div class="row">
	                <div class="col col-lg-6">
	
	                    <?php the_content(); ?>
	
	                </div><!-- /.col -->
	                <div class="col col-lg-6">
	
	                    <h3>Onde estamos</h3>
	
	                    <p>
	                        <?php the_field('endereco_completo'); ?>
	                    </p>
	
	                    <div class="separator separator-20"></div>
	
	                    <img src="<?php the_field('imagem_google_maps'); ?>" alt="<?php the_field('endereco_completo'); ?>" style="width: 100%;">
	
	                </div><!-- /.col -->
	            </div><!-- /.row -->
	
	            <div class="separator separator-40"></div>
	
	        </div><!-- /.container -->
	    </div><!-- /.areas -->

	<?php endwhile; ?>

<?php get_footer(); ?>