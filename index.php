<?php get_header(); ?>

    <div class="main-banner hidden-xs">

        <div id="lms-slider" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
            	<?php
            	$c=0;
        		while ( have_rows('banner_repeater','option') ) { the_row(); ?>
	                <li data-target="#lms-slider" data-slide-to="<?php echo $c; ?>" <?php if ( $c == 0 ) { ?> class="active" <?php } ?>></li>
                	<?php ++$c;
				} ?>
            </ol>

            <div class="carousel-inner" role="listbox">
            	
            	<?php if ( have_rows('banner_repeater','option') ) {
            		$i=0; ?>

					<?php while ( have_rows('banner_repeater','option') ) {
						the_row(); ?>

                		<div class="item <?php if ( $i == 0 ) echo 'active'; ?>" style="background-image: url(<?php the_sub_field('imagem_do_banner','option'); ?>);">

		                    <div class="main-banner-overlay"></div>

		                    <div class="carousel-caption">
		                        <div class="container">

		                            <?php the_sub_field('texto_do_banner','option'); ?>

		                            <div class="separator separator-10"></div>

		                            <a href="<?php the_sub_field('link_do_botao_do_banner','option'); ?>" class="btn btn-warning"><?php the_sub_field('texto_do_botao_do_banner','option'); ?></a>

		                        </div>
		                    </div>
		                </div>

            			<?php
            			 ++$i;
					} ?>

				<?php } ?>

            </div><!-- /.carousel-inner -->

        </div><!-- /.carousel -->

    </div><!-- /.main-banner -->

    <div class="areas padding-vertical-40">
        <div class="container">

            <div class="text-center">
                <h3>Áreas de Atuação</h3>
                <span class="h-separator"></span>
            </div>

            <div class="separator separator-40"></div>

            <div class="row">

				<?php if ( have_rows('categorias_dos_produtos','option') ) { ?>

					<?php while ( have_rows('categorias_dos_produtos','option') ) {
						the_row(); ?>

                		<div class="col col-xs-12 col-sm-6 col-lg-3 the-category">

							<?php
							    $category_id = get_cat_ID( get_sub_field('nome_da_categoria','option') );
							    $category_link = get_category_link( $category_id );
							?>
		                    <a href="<?php echo esc_url( $category_link ); ?>">
			                    <div class="the-category-cover" style="background-image: url(<?php the_sub_field('foto_de_capa_da_categoria','option'); ?>);">
			                        <div class="the-category-name"><?php the_sub_field('nome_da_categoria','option'); ?></div>
			                    </div>
		                    </a>

		                </div><!-- /.col -->

        			<?php } ?>

				<?php } ?>

            </div><!-- /.row -->

            <div class="separator separator-40"></div>

        </div><!-- /.container -->
    </div><!-- /.areas -->

    <div class="company-values">

        <div class="company-values-overlay"></div>

        <div class="container company-values-content">

            <div class="separator separator-40"></div>

            <div class="row">
                <div class="col col-xs-12 col-sm-6 col-lg-6 text-center">

                    <h3>Nossa Visão</h3>
                    <span class="h-separator"></span>

                    <div class="separator separator-40"></div>

                    <?php the_field('visao_content','option'); ?>

                </div><!-- /.col -->
                <div class="col col-xs-12 col-sm-6 col-lg-6 text-center">

                    <h3>Nossa Missão</h3>
                    <span class="h-separator"></span>

                    <div class="separator separator-40"></div>

                    <?php the_field('missao_content','option'); ?>

                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.company-values -->

    <div class="partners padding-vertical-40">
        <div class="container">

            <div class="text-center">
                <h3>Parceiros</h3>
                <span class="h-separator"></span>
            </div>

            <div class="separator separator-40"></div>

            <div class="row">

            	<?php if ( have_rows('parceiros_repeater','option') ) { ?>

					<?php while ( have_rows('parceiros_repeater','option') ) {
						the_row(); $i++; ?>

                		<div class="col col-xs-6 col-lg-3 the-partner">

		                    <a href="<?php the_sub_field('url_do_site_parceiro','option'); ?>" target="_blank">
			                    <div class="the-partner-logo" style="background-image: url(<?php the_sub_field('logo_do_parceiro','option'); ?>);"></div>
			                    <?php
			                    $the_url =  get_sub_field('url_do_site_parceiro','option');
								$the_url = preg_replace('/(http)(s?)(\:\/\/)/', '', $the_url);
								echo $the_url;
			                    ?>
		                    </a>

                		</div><!-- /.col -->
            		<?php } ?>

				<?php } ?>

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.areas -->

    <div class="locations padding-vertical-40">
        <div class="container">

            <div class="text-center">
                <h3>Unidades CPE</h3>
                <span class="h-separator"></span>
            </div>

            <div class="separator separator-40"></div>

            <?php if ( have_rows('unidades_repeater','option') ) { ?>

				<div class="row">

					<?php while ( have_rows('unidades_repeater','option') ) {
						the_row(); $i++; ?>

	            		<div class="col col-xs-6 col-lg-3 the-partner">

		                    <a href="<?php the_sub_field('url_da_unidade','option'); ?>" target="_blank">
			                    <div class="the-partner-logo" style="background-image: url(<?php the_sub_field('logo_da_unidade','option'); ?>);"></div>
			                    <?php the_sub_field('regiao_da_unidade','option'); ?>
		                    </a>

                		</div><!-- /.col -->

	        		<?php } ?>

        		</div>

			<?php } ?>

        </div><!-- /.container -->
    </div><!-- /.areas -->

<?php get_footer(); ?>