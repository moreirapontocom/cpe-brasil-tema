	<footer>
        <div class="container">

            <div class="padding-vertical-40">

                <div class="row">
                    <!--
                    <div class="col col-xs-12 col-sm-4 col-lg-5">
                        <h4>title</h4>

                        conteudo

                        <div class="separator separator-30"></div>

                        <img src="<?php echo get_template_directory_uri(); ?>/img/footer-logos.png" alt="CPE do Brasil">
                    </div><!-- /.col -->

                    <?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>

						<?php dynamic_sidebar( 'footer-widget-1' ); ?>

					<?php endif; ?>

                    <div class="separator separator-100 visible-xs"></div>

                    <?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>

						<?php dynamic_sidebar( 'footer-widget-2' ); ?>

					<?php endif; ?>

                    <div class="separator separator-50 visible-xs"></div>

                    <?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>

						<?php dynamic_sidebar( 'footer-widget-3' ); ?>

					<?php endif; ?>
                </div><!-- /.row -->

            </div><!-- /.padding-vertical-40 -->

        </div><!-- /.container -->
        <div class="copyright">
            <div class="container">

                <div class="pull-left">
                    &copy; <?php echo date('Y'); ?>. <?php bloginfo('name'); ?>.
                </div>
                <div class="pull-right">
                    <a href="#">
                        Desenvolvido por...
                    </a>
                </div>

            </div><!-- /.container -->
        </div><!-- /.copyright -->
    </footer>

    <script>
        $(function() {
            $('.company-values-overlay, .company-values-content').css('height', $('.company-values').height() + 'px' );
        });
    </script>

	<?php wp_footer(); ?>

</body>
</html>