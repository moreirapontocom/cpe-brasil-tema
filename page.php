<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="main-banner-page">

	        <div class="main-banner main-banner-page-bg"></div><!-- /.main-banner -->

	        <div class="text-center main-banner-page-content">
	            <h3><?php the_title(); ?></h3>
	            <span class="h-separator"></span>
	        </div>

	    </div>

	    <div class="separator separator-50"></div>

	    <div class="areas padding-vertical-40">
	        <div class="container">

	            <?php the_content(); ?>

	            <div class="separator separator-40"></div>

	        </div><!-- /.container -->
	    </div><!-- /.areas -->

	<?php endwhile; ?>

<?php get_footer(); ?>