<div class="sidebar">

	<?php if ( get_field('utilizar_bio_do_autor','option') == 1 ) : ?>

		<div class="sidebar-widget">

			<?php if ( get_field('autor_foto','option') != '' ) : ?>

			    <div class="text-center">
			        <img src="<?php the_field('autor_foto','option'); ?>" alt="Sobre mim" class="img-circle">
			    </div>

				<div class="separator separator-20"></div>

			<?php endif; ?>

	        <?php the_field('autor_bio','option'); ?>

			<div class="separator separator-20"></div>

		    <a href="<?php the_field('autor_link','option'); ?>">&raquo; Leia mais</a>
		</div>

	<?php endif; ?>

	<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>

		<?php dynamic_sidebar( 'right-sidebar' ); ?>

	<?php endif; ?>

	<?php if ( get_field('utilizar_newsletter_mailchimp','option') == 1 && get_field('newsletter_url_mailchimp','option') != '' ) : ?>

	    <div id="sidebar-subscription" data-spy="affix" class="visible-lg">

			<script>
				$(function() {
					var content = '<?php echo str_replace(PHP_EOL, '', get_field('texto_newsletter','option')); ?>',
						alt_content = '<?php ( get_field('texto_alternativo_news_sidebar','option') != '' ) ? $content = get_field('texto_alternativo_news_sidebar','option') : $content = get_field('texto_newsletter','option'); echo str_replace(PHP_EOL, '', $content); ?>';

					if ( Cookies.get('wef_subscripted') != undefined ) {
						<?php if ( get_field('texto_alternativo_news_sidebar','option') != '' ) { ?>
							$('#sidebar_newsletter_content').html( alt_content );
							$('#sidebar_newsletter_form').hide();
						<?php } ?>
					}
				});
			</script>

			<span id="sidebar_newsletter_content"></span>

	        <div id="sidebar_newsletter_form">
		        <form onsubmit="before_subscription()" action="<?php echo the_field('newsletter_url_mailchimp','option'); ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

			        <h2 class="widget-title"><?php the_field('texto_newsletter','option'); ?></h2>
			        <p>
			            Coloque seu melhor email aqui
			        </p>
			        <p>
			            <input type="email" value="" name="EMAIL" class="required email search-field form-control" id="mce-EMAIL" placeholder="Seu email">
			        </p>

			        <div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>
					<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8a2b906c22707dde5c2c005aa_4ba087d7bf" tabindex="-1" value=""></div>
					<div class="separator"></div>
					<p>
						<input type="submit" value="<?php the_field('texto_newsletter_botao','option'); ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default btn-lg btn-subscribe btn-subscribe-sidebar">
					</p>

		        </form>
	        </div>

	    </div>

    <?php endif; ?>

</div><!-- /.sidebar -->