<div class="post-details">
    <span class="hidden-xs">Escrito por </span>
    	<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
    		<?php the_author_meta( 'display_name' ); ?>
		</a> em <?php the_time(get_option('date_format')); ?>

	<br>

	<?php if ( comments_open() ) : ?>
		<a href="<?php echo the_permalink(); ?>#disqus_thread">Comentários</a>
	<?php endif; ?>

	<?php edit_post_link(__('Edit', 'web-em-foco'), ' &mdash; ') ?>
</div>